<?php

/**
 * @file
 * Administration page callbacks for the hyphenator module.
 */

/**
 * Admin hyphenator module form.
 */
function hyphenator_admin($form, &$form_state) {

  // Prologue settings:
  $form['prologue'] = array('#type' => 'fieldset', '#title' => t('Prologue'));
  $form['prologue']['hyphenator_hyphenator_js_script_present'] = array(
    '#type' => 'checkbox',
    '#disabled' => TRUE,
    '#title' => t('<em>Hyphenator.js</em> library present?'),
    '#default_value' => _hyphenator_hyphenator_js_file::getExists() ? 1 : 0,
    '#description' => !_hyphenator_hyphenator_js_file::getExists() ?
      t('This checkbox cannot be modified here. It will be checked once you have installed manually the <em>Hyphenator.js</em> library in \'!hyphenator_file\'. Please find this javascript library at <a href="!hyphenator_url">!hyphenator_url</a>.',
        array('!hyphenator_file' => _hyphenator_hyphenator_js_file::getPath(), '!hyphenator_url' => url('http://code.google.com/p/hyphenator/'))) :
      t("Hyphenator.js version '%v' installed (path: '%p')!",
        array('%v' => _hyphenator_hyphenator_js_file::getVersion(), '%p' => _hyphenator_hyphenator_js_file::getPath()))
    ,
  );

  // Where Hyphenator.js must operate settings:
  $form['where'] = array('#type' => 'fieldset', '#title' => t('Configure where <em>Hyphenator.js</em> must operate'),
    '#description' => t('There is 3 levels on which you can tell <em>Hyphenator.js</em> to operate: <em>All</em>, <em>Node Types</em> and <em>Nodes</em>. Inheritance setting is possible and start from <em>Nodes</em>, to <em>Node Type</em> and <em>All</em>.'));
  $form['where']['hyphenator_hyphenate_all'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hyphenate all'),
    '#default_value' => variable_get('hyphenator_hyphenate_all', TRUE),
    '#description' => t('This setting will be propaged to nodes that are set to inherit (see bellow).')
  );
  $form['where']['nodes_type'] = array('#type' => 'fieldset', '#title' => t('Hyphenate node types'), '#collapsible' => TRUE, '#collapsed' => TRUE, '#description' => t("Choosing a value else than 'inherit' will override the 'Hyphenate all' setting!"));
  $node_types = node_type_get_types();
  uasort($node_types, create_function('$a,$b', 'return strcmp($a->name, $b->name);'));
  foreach ($node_types as $node_type) {
    $form['where']['nodes_type']['hyphenator_hyphenate_by_type_' . $node_type->type] = array(
      '#type' => 'select',
      '#title' => $node_type->name,
      '#default_value' => variable_get('hyphenator_hyphenate_by_type_' . $node_type->type, 'inherit'),
      '#options' => array('inherit' => t('Inherit'), 'yes' => t('Yes'), 'no' => t('No')),
    );
  }
  $form['where']['hyphenator_hyphenate_at_node_level'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow to hyphenate at node level'),
    '#default_value' => variable_get('hyphenator_hyphenate_at_node_level', 0),
    '#description' => t('It will add a field <em>hyphenate</em> to be set with the values <em>yes</em>, <em>no</em> or <em>inherit</em> on a node basis.')
  );

  // Where Hyphenator.js must operate settings:
  $form['how'] = array('#type' => 'fieldset', '#title' => t('Configure how <em>Hyphenator.js</em> must operate'),
    '#description' => t('Those parameters permit to customize how <em>Hyphenator.js</em> must operate (see !hyphenator_site for full documentation).',
      array('!hyphenator_site' => l('Hyphenator.js web site', 'http://code.google.com/p/hyphenator/'))));
  $form['how']['hyphenator_hyphenator_js_config'] = array('#type' => 'fieldset', '#collapsible' => TRUE, '#collapsed' => TRUE, '#title' => t('Config parameters'));
  $hyphenator_js_config = variable_get('hyphenator_hyphenator_js_config', array());
  foreach (_hyphenator_get_hyphenator_js_default_config () as $key => $value) {
    if (array_key_exists('list', $value)) {
      $form['how']['hyphenator_hyphenator_js_config']['hyphenator_hyphenator_js_config_' . $key] = array(
        '#type' => 'select',
        '#options' => $value['list'],
      );
    }
    else {
      switch ($value['type']) {
        case 'string':
        case 'number':
          $form['how']['hyphenator_hyphenator_js_config']['hyphenator_hyphenator_js_config_' . $key] = array(
            '#type' => 'textfield',
          );
          break;
        case 'boolean':
          $form['how']['hyphenator_hyphenator_js_config']['hyphenator_hyphenator_js_config_' . $key] = array(
            '#type' => 'select',
            '#options' => array(
              'true' => t('true!d', array('!d' => ($value['default'] == 'true') ? ' (default)' : '')),
              'false' => t('false!d', array('!d' => ($value['default'] == 'false') ? ' (default)' : ''))),
          );
          break;
      }
    }
    if (array_key_exists('hyphenator_hyphenator_js_config_' . $key, $form['how']['hyphenator_hyphenator_js_config'])) {
      $form['how']['hyphenator_hyphenator_js_config']['hyphenator_hyphenator_js_config_' . $key]['#title'] = $key;
      $form['how']['hyphenator_hyphenator_js_config']['hyphenator_hyphenator_js_config_' . $key]['#default_value'] = isset($hyphenator_js_config[$key]) ? $hyphenator_js_config[$key]['value'] : $value['default'];
      $form['how']['hyphenator_hyphenator_js_config']['hyphenator_hyphenator_js_config_' . $key]['#description'] = $value['descr'];
    }
  }

  // Hyphenate.js addExceptions
  $form['how']['hyphenator_hyphenator_js_exceptions'] = array('#type' => 'fieldset', '#collapsible' => TRUE, '#collapsed' => FALSE, '#title' => t('Exceptions'), '#description' => t('Add words here that you find are wrongly hyphenated by <em>Hyphenator.js</em>.'));
  hyphenator_hyphenator_js_exception_overview_form($form['how']['hyphenator_hyphenator_js_exceptions']);

  // Epilogue:
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  // By default, render the form using theme_system_settings_form().
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'system_settings_form';
  }

  return $form;
}

/**
 * Overview of the recorded Hyphenator.js exceptions.
 */
function hyphenator_hyphenator_js_exception_overview_form(&$form) {

  $rows = array();
  $exceptions = variable_get('hyphenator_hyphenator_js_exceptions', array());

  foreach ($exceptions as $key => $value) {
    $rows[] = array(
      $key,
      $value,
      l(t('edit'), 'admin/config/content/hyphenator/hyphenator_js_exception_mod/' . $key),
      l(t('delete'), 'admin/config/content/hyphenator/hyphenator_js_exception_del/' . $key),
    );
  }
  $form['hyphenator_js_exceptions_add'] = array(
    '#type' => 'item',
    '#theme' => 'links', // The following 2 fields are the theme_links() arguments.
    '#links' => array(
      'add' => array('title' => t('Add an exception'), 'href' => 'admin/config/content/hyphenator/hyphenator_js_exception_add')
    ),
    '#attributes' => array('class' => 'action-links'),
  );
  $form['hyphenator_js_exceptions_table'] = array(
    '#type' => 'markup',
    '#theme' => 'table', // The following 3 fields are the theme_table() arguments.
    '#header' => array(t('Language'), t('Words'), array('data' => t('Operations'), 'colspan' => 2)),
    '#empty' => t('no exceptions recorded'),
    '#rows' => $rows,
  );
}

/**
 * Process admin hyphenator module form submission.
 */
function hyphenator_admin_submit($form, $form_state) {

  variable_set('hyphenator_hyphenate_all', $form_state['values']['hyphenator_hyphenate_all']);
  variable_set('hyphenator_hyphenate_at_node_level', $form_state['values']['hyphenator_hyphenate_at_node_level']);
  // Remove/Insert field allowing to hyphenate at node level:
  $node_types = node_type_get_types();
  foreach ($node_types as $key => $value) {
    $instance = field_info_instance('node', HYPHENATOR_FIELD_NAME, $key);
    if ($form_state['values']['hyphenator_hyphenate_at_node_level']) {
      if (empty($instance)) {
        $instance = array(
          'field_name' => HYPHENATOR_FIELD_NAME,
          'entity_type' => 'node',
          'bundle' => $key,
          'label' => t('Hyphenate content'),
          'description' => t("Will override any upper settings if set to a value different than <em>Inherit</em> (see !page_conf).",
            array('!page_conf' => l(t('Administration » Configuration » Content authoring » Hyphenator'), 'admin/config/content/hyphenator'))),
          'required' => FALSE,
          'default_value' => array(array('value' => 'inherit')),
          'display' => array(
            'default' => array(
              'type' => 'hidden'
            ),
            'teaser' => array(
              'type' => 'hidden'
            )
          ),
        );
        $instance = field_create_instance($instance);
        watchdog('Hyphenator', 'Added hyphenate field to content type: %key',
          array('%key' => $key));
      }
    }
    else {
      if (!empty($instance)) {
        field_delete_instance($instance, FALSE);
        watchdog("Hyphenator", 'Deleted hyphenate field from content type: %key', array('%key' => $key));
      }
    }
  }

  // Save hyphenator.js config in a array:
  $hyphenator_js_config = array();
  foreach (_hyphenator_get_hyphenator_js_default_config () as $key => $value) {
    if ($form_state['values']['hyphenator_hyphenator_js_config_' . $key] <> $value['default']) {
      $hyphenator_js_config[$key] = array(
        'type' => $value['type'],
        'value' => $form_state['values']['hyphenator_hyphenator_js_config_' . $key],
      );
    }
  }
  variable_set('hyphenator_hyphenator_js_config', $hyphenator_js_config);

  // Save hyphenate by node type settings  
  $hyphenate_by_node_type = array();
  foreach ($form_state['values'] as $key => $value) {
    if (strpos($key, 'hyphenator_hyphenate_by_type_') === 0) {
      variable_set($key, $value);
    }
  }

  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Process admin hyphenator module form validation.
 */
function hyphenator_admin_validate($form, $form_state) {

  if (!preg_match('/^\w+$/', $form_state['values']['hyphenator_hyphenator_js_config_classname'])) {
    form_set_error('hyphenator_hyphenator_js_config_classname',
      t("The 'classname' parameter must be set with a non-spacing string"));
  }
  if (!preg_match('/^\S?$/', $form_state['values']['hyphenator_hyphenator_js_config_hyphenchar'])) {
    form_set_error('hyphenator_hyphenator_js_config_hyphenchar',
      t("The 'hyphenchar' parameter may be set with a visible char"));
  }
  if (!preg_match('/^\d+$/', $form_state['values']['hyphenator_hyphenator_js_config_minwordlength'])) {
    form_set_error('hyphenator_hyphenator_js_config_minwordlength',
      t("The 'minwordlength' parameter must be set with a number"));
  }
}

/**
 * Hyphenator.js exceptions form editor.
 */
function hyphenator_hyphenator_js_exception_edit_form($form, &$form_state, $language = NULL) {

  if (is_null($language)) {
    $language = 'GLOBAL';
    $words = '';
    $new = TRUE;
  }
  else {
    $exceptions = variable_get('hyphenator_hyphenator_js_exceptions', array());
    $words = (array_key_exists($language, $exceptions)) ? $exceptions[$language] : '';
    $new = FALSE;
  }

  $languages = _hyphenator_get_hyphenator_js_exception_langages();
  if (!empty($languages)) {
    $languages = array_merge(array('GLOBAL' => 'GLOBAL'), $languages);
  }
  else {

  }

  $form['message'] = array('#markup' =>
    ($new) ? t('Add one or more exception words for a specified language.') : t('Modifiy the exception words list for the specified language.'));

  $form['language'] = array(
    '#type' => empty($languages) ? 'textfield' : 'select',
    '#size' => 10,
    '#maxlength' => 15,
    '#title' => t('Language'),
    '#disabled' => !$new,
    '#description' => t('Use \'GLOBAL\' !blato enable the associated words to be skipped for any languages.',
      array('!bla' => empty($languages) ? t('or an empty string') . ' ' : '')),
    '#default_value' => $language,
  );
  if (!empty($languages)) {
    $form['language']['#options'] = $languages;
  }
  $form['exception_words'] = array(
    '#type' => 'textarea',
    '#title' => t('Exception words'),
    '#description' => t('Begin a newline to add a word.'),
    '#default_value' => $words,
  );
  $form['exception_new'] = array(
    '#type' => 'hidden',
    '#value' => $new,
  );

  // Epilogue:
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save exception'));
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/content/hyphenator',
  );

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }

  // By default, render the form using theme_system_settings_form().
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'system_settings_form';
  }

  return $form;
}

/**
 * Process Hyphenator.js exceptions form editor submission.
 */
function hyphenator_hyphenator_js_exception_edit_form_submit($form, &$form_state) {

  $form_state['redirect'] = 'admin/config/content/hyphenator';

  $exceptions = variable_get('hyphenator_hyphenator_js_exceptions', array());
  $exceptions[empty($form_state['values']['language']) ? 'GLOBAL' : $form_state['values']['language']] = trim($form_state['values']['exception_words']);
  variable_set('hyphenator_hyphenator_js_exceptions', $exceptions);

  drupal_set_message(t("The hyphenator exception language '%l' have been saved.", array('%l' => ($form_state['values']['language'] == '') ? 'GLOBAL' : $form_state['values']['language'])));
}

/**
 * Process Hyphenator.js exceptions form editor validation.
 */
function hyphenator_hyphenator_js_exception_edit_form_validate($form, $form_state) {

  $submitted_language = $form_state['values']['language'];
  $submitted_language = ($submitted_language == '') ? 'GLOBAL' : $submitted_language;
  $new_record = $form_state['values']['exception_new'];
//  if (!preg_match('/^(\w\w)?$/', $form_state['values']['language'])) {
//    form_set_error('language',
//            t("The 'Language' field may be set with a string of two char"));
//  }
  $exceptions = variable_get('hyphenator_hyphenator_js_exceptions', array());
  if (array_key_exists($submitted_language, $exceptions) && $new_record) {
    form_set_error('language',
      t("The Language %x that you submitted already exists.",
        array('%x' => $submitted_language)));
  }
  if (!preg_match('/^(\s*\S+\s*)+$/', $form_state['values']['exception_words'])) {
    form_set_error('exception_words',
      t("The 'Exception words' field must be set with some strings."));
  }
}

/**
 * Hyphenator.js exceptions form delete.
 */
function hyphenator_hyphenator_js_exception_delete_form($form, &$form_state, $language) {

  $exceptions = variable_get('hyphenator_hyphenator_js_exceptions', array());

  $words = (array_key_exists($language, $exceptions)) ? $exceptions[$language] : '';

  $form['language'] = array('#type' => 'hidden', '#value' => $language);

  return confirm_form($form,
    t('Are you sure you want to delete de Hyphenator language %l and its exception(s)?', array('%l' => $language)),
    'admin/config/content/hyphenator',
    t('<p>The language %l hold the following exception word(s): %w.</p><p>This action cannot be undone.</p>', array('%l' => $language, '%w' => $words)),
    t('Delete'), t('Cancel'));
}

/**
 * Process Hyphenator.js exceptions form delete submission.
 */
function hyphenator_hyphenator_js_exception_delete_form_submit($form, &$form_state) {

  $form_state['redirect'] = 'admin/config/content/hyphenator';

  $exceptions = variable_get('hyphenator_hyphenator_js_exceptions', array());
  unset($exceptions[empty($form_state['values']['language']) ? 'GLOBAL' : $form_state['values']['language']]);
  variable_set('hyphenator_hyphenator_js_exceptions', $exceptions);

  drupal_set_message(t("The hyphenator language %l and its exception(s) have been deleted.", array('%l' => $form_state['values']['language'])));
}